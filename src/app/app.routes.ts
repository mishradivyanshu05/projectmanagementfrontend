import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { HomeComponent } from '../app/components/home/home.component';
import { ProjectComponent } from '../app/components/project/project.component';

const routes: Routes =
    [
        { path: 'home', component: HomeComponent },
        { path: '', redirectTo: 'home', pathMatch: 'full' },
        { path: 'project', loadChildren: './components/project/project.module#ProjectModule' },
    ];


@NgModule({
    imports: [RouterModule.forRoot(routes, { useHash: false, onSameUrlNavigation: 'reload' })],
    exports: [RouterModule]
})

export class AppRoutingModule {

}
