import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import {environment} from '../../../environments/environment';
@Injectable()
export class HomeService {
    constructor(private httpClient: HttpClient) { }


    getProject() : Observable<any>{
        return this.httpClient.get(`${environment.serviceBaseURL}/getProjects/` );
      }
    createProject(details){
        return this.httpClient.post(`${environment.serviceBaseURL}/CreateProject/` ,details);
    }
    getProjectDetail(id) : Observable<any>{
        return this.httpClient.get(`${environment.serviceBaseURL}/ProjectDetail/${id}/` );
      }
    editProjectDetail(id,details) : Observable<any>{
        return this.httpClient.put(`${environment.serviceBaseURL}/CreateProject/${id}/` ,details );
      }
    deleteProject(id) : Observable<any>{
        return this.httpClient.delete(`${environment.serviceBaseURL}/CreateProject/${id}/` );
      }
    geTask(id) : Observable<any>{
      return this.httpClient.get(`${environment.serviceBaseURL}/Task/${id}/`)
    }
    createTask(id,taskdetails) : Observable<any>{
      return this.httpClient.post(`${environment.serviceBaseURL}/Task/${id}/` ,taskdetails)
    }
    getTaskDetail(id) : Observable<any>{
        return this.httpClient.get(`${environment.serviceBaseURL}/TaskDetail/${id}/` );
    }
    editTaskDetail(id,details) : Observable<any>{
        return this.httpClient.put(`${environment.serviceBaseURL}/TaskDetail/${id}/` ,details );
    }
    deleteTask(id) : Observable<any>{
        return this.httpClient.delete(`${environment.serviceBaseURL}/TaskDetail/${id}/` );
    }
}
