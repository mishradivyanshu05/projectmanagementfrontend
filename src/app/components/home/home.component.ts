import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HomeService } from './home.services'
import { ModalDirective } from 'ngx-bootstrap/modal';
import { getProject,createProject } from './home.entity';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  //getproject : getProject;
  createproject : createProject;
  getproject: any;
  @ViewChild('childModal') childModal: ModalDirective;
  constructor(
    private hs: HomeService,
    private router : Router) {
    //this.getproject = new getProject();
    this.createproject = new createProject();

    // change the date time
  }

  ngOnInit() {
    this.getProjectsDetail();
  }
  hideChildModal(): void {
        this.childModal.hide();
    }

  getProjectsDetail() {
    this.hs.getProject().subscribe(res => {
      if (res) {
        console.log(res);
        if (res) {
          this.getproject = res.projectlist;
        }
      }
    })
  }
  saveProjectDetails() {
        console.log(this.createproject)
        if (this.createproject != null){
          this.hs.createProject(this.createproject).subscribe(res => {
            console.log("request made");
            console.log(res);
            window.location.href = '/home';
            this.router.navigate(["/home"])
          })
        }
    }
  showModal() {
                this.childModal.show();
    }
    skip() {
          console.log("inskip");
          window.location.href = '/home';
          this.router.navigate(["/home"]);
      }
}
