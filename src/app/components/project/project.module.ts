import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {HomeService} from '../home/home.services'
import { ViewProjectComponent } from './view/view.component';
import { TaskComponent } from '../task/task.component';
//import { AddItemComponent } from './add/add.component';
import { ProjectComponent } from './project.component';
import { ModalModule } from 'ngx-bootstrap';

@NgModule({
    declarations: [
      ProjectComponent,
      TaskComponent,
      ViewProjectComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        ModalModule.forRoot(),
        ReactiveFormsModule,
        RouterModule.forChild([
            {
                path: '', component: ProjectComponent,
                children: [
                    { path: ':id1/task/:id2', component: TaskComponent },
                    { path: ':id', component: ViewProjectComponent },
                    { path: ':id', redirectTo: ':id' }
                ]
            }
        ])
    ],
    exports: [
    ],
    providers: [
        HomeService,
    ],
})

// This class is our root module of this application therefore all routes passes through here.
export class ProjectModule { }
