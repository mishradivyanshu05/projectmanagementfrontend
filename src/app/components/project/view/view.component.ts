import { Component, OnInit, ViewChild } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import {HomeService} from '../../home/home.services'
import { ModalDirective } from 'ngx-bootstrap/modal';
import { getProject,createProject,createTask } from './view.entity';

@Component({
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewProjectComponent implements OnInit {
  id :number;
  task:any;
  projectdetail:any;
  taskdetails:any;
  burl:string;
  @ViewChild('taskchildModal') private taskchildModal: ModalDirective;
  constructor(
    private hs: HomeService,
    private activatedRoute: ActivatedRoute) {
      this.projectdetail = new createProject(),
      this.taskdetails = new createTask()
     }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.params['id']
    console.log(this.id);
     console.log(this.activatedRoute.snapshot['_routerState'].url); 
      this.hs.getProjectDetail(this.id).subscribe(res => {
        this.projectdetail = res;
      })
    this.hs.geTask(this.id).subscribe(res => {
      this.task = res
    })
    this.burl = "/project/"+this.id+"/task/";
    }
    saveProjectDetails(){
      this.hs.editProjectDetail(this.id,this.projectdetail).subscribe(res => {
        console.log(res);
        window.location.href = '/home';
      })
    }
    saveTaskDetails(){
      this.hs.createTask(this.id,this.taskdetails).subscribe(res => {
        console.log(res);
        window.location.reload();
      })
    }

    showModal() {
          this.taskchildModal.show();
      }
    hideChildModal(): void {
          this.taskchildModal.hide();
      }
    skip() {
          console.log("inskip");
          window.location.reload();
          //this.router.navigate(["/home"]);
      }
    skipp() {
          console.log("inskipp");
          window.location.href = '';
          //this.router.navigate(["/home"]);
      }

    delete(){
      this.hs.deleteProject(this.id).subscribe(res => {
        console.log(res);
        window.location.href = '/home';
      })
    }
    edittask(){

    }
  }
