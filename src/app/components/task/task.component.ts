import { Component, OnInit, ViewChild  } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import {HomeService} from '../home/home.services'
import { ModalDirective } from 'ngx-bootstrap/modal';
import { createTask } from './task.entity';
import {DatePipe} from '@angular/common';

@Component({
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent implements OnInit {
  taskdetail:any;
  projectid:any;
  taskid:any;
  constructor(
    private hs: HomeService,
    private activatedRoute: ActivatedRoute) {
      this.taskdetail = new createTask()
     }

  ngOnInit() {
    this.projectid= this.activatedRoute.snapshot.params['id1']
    this.taskid = this.activatedRoute.snapshot.params['id2']
    console.log(this.projectid,this.taskid);
    //  console.log(this.activatedRoute.snapshot['_routerState'].url);
    this.hs.getTaskDetail(this.taskid).subscribe(res => {
      this.taskdetail = res;
    })
  }
    // this.hs.geTask(this.id).subscribe(res => {
    //   this.task = res
    // })
    // this.burl = "/project/"+this.id+"/task/";
    // }
    saveTaskDetail(){
      //console.log(res);
      this.hs.editTaskDetail(this.taskid,this.taskdetail).subscribe(res => {
        window.location.href = 'project/'+this.projectid;
      })
    }
    delete(){
      this.hs.deleteTask(this.taskid).subscribe(res => {
        console.log(res);
        window.location.href = 'project/'+this.projectid;
      })
    }
    skipp() {
          window.location.href = 'project/'+this.projectid;
          //this.router.navigate(["/home"]);
    }
}
